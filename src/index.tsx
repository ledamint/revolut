import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import {store} from './store';
import './index.css';
import {ConnectedApp} from './components/App';

ReactDOM.render(
    <Provider store={store}><ConnectedApp /></Provider>,
    document.getElementById('root')
);
