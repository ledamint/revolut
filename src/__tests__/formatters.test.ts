import {formatMoney} from '../formatters';
import {Currency} from '../config';

describe('formatters', () => {
    test('formatMoney', () => {
        expect(formatMoney(10, Currency.EUR)).toEqual('€10');
        expect(formatMoney(0, Currency.USD)).toEqual('$0');
        expect(formatMoney(55.89, Currency.GBP)).toEqual('£55.89');
    });
});
