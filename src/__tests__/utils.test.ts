import {round2decimal, calculateCircularIndex} from '../utils';

describe('utils', () => {
    test('round2decimal', () => {
        expect(round2decimal(100)).toEqual(100);
        expect(round2decimal(-100)).toEqual(-100);
        expect(round2decimal(0)).toEqual(0);
        expect(round2decimal(100.1)).toEqual(100.1);
        expect(round2decimal(100.01)).toEqual(100.01);
        expect(round2decimal(100.011)).toEqual(100.01);
        expect(round2decimal(100.017)).toEqual(100.02);
        expect(round2decimal(100.999)).toEqual(101);
    });

    test('calculateCircularIndex', () => {
        expect(calculateCircularIndex(0, [0,1,2])).toEqual(0);
        expect(calculateCircularIndex(1, [0,1,2])).toEqual(1);
        expect(calculateCircularIndex(2, [0,1,2])).toEqual(2);
        expect(calculateCircularIndex(-1, [0,1,2])).toEqual(2);
        expect(calculateCircularIndex(3, [0,1,2])).toEqual(0);
    });
});
