export const FXRatesApi = 'http://data.fixer.io/api/latest?access_key=888db26d5ff1443986ed651552ad3cd3&symbols=USD,GBP&format=1';

export enum Currency {
    USD = 'USD',
    EUR = 'EUR',
    GBP = 'GBP',
}
