import {GlobalState, MoneyBalance} from './types';
import {Currency} from '../config';

export const getMoneyBalance = (state: GlobalState): MoneyBalance => {
    return state.moneyBalance;
};

export const getAvailableToTransferCurrencies = (state: GlobalState): Currency[] => {
    return Object.keys(state.moneyBalance).sort() as Currency[];
};

export const getFXRates = (state: GlobalState) => {
    return state.FXRates;
};

