import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import {EXCHANGE, exchange, UPDATE_FX_RATES, updateFXRates} from '../actions';
import {Currency, FXRatesApi} from '../../config';
import {ExchangeActionPayload} from '../types';

describe('actions', () => {
    test('exchange', () => {
        const exchangeActionPayload: ExchangeActionPayload = {
            depositMoney: 0,
            withdrawalMoney: 0,
            withdrawalCurrency: Currency.EUR,
            depositCurrency: Currency.GBP,
        };

        const expectedAction = {
            type: EXCHANGE,
            payload: exchangeActionPayload,
        };

        expect(exchange(exchangeActionPayload)).toEqual(expectedAction);
    });
});

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {
    afterEach(() => {
        fetchMock.restore()
    });

    test('updateFXRates', async () => {
        const body = {
            success: true,
            timestamp: 1558256525,
            base: 'EUR',
            date: '2019-05-19',
            rates: {
                USD: 1.116744,
                GBP: 0.878085
            }
        };
        const expectedActions = [{
            type: UPDATE_FX_RATES,
            payload: {
                EUR: 1,
                USD: 1.116744,
                GBP: 0.878085
            },
        }];
        fetchMock.getOnce(FXRatesApi, {body});
        const store = mockStore();
        // @ts-ignore
        await store.dispatch(updateFXRates());
        expect(store.getActions()).toEqual(expectedActions);
    });
});
