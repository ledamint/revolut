import {getFXRates, getAvailableToTransferCurrencies, getMoneyBalance} from '../selectors';
import {initialState} from '../reducer';
import {Currency} from '../../config';

describe('selectors' , () => {
    test('getFXRates', () => {
        expect(getFXRates(initialState)).toEqual(initialState.FXRates);
    });

    test('getMoneyBalance', () => {
        expect(getMoneyBalance(initialState)).toEqual(initialState.moneyBalance);
    });

    test('getAvailableToTransferCurrencies', () => {
        const state = {
            ...initialState,
            moneyBalance: {
                [Currency.USD]: 0,
                [Currency.GBP]: 0,
                [Currency.EUR]: 0,
            }
        };
        expect(getAvailableToTransferCurrencies(state)).toEqual([Currency.EUR, Currency.GBP, Currency.USD]);
    });
});
