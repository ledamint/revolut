import {FXRates, moneyBalance} from '../reducer'
import {EXCHANGE, UPDATE_FX_RATES} from '../actions';
import {Currency} from '../../config';

describe('FXRates reducer', () => {
    test('UPDATE_FX_RATES', () => {
        const rates = {
            [Currency.USD]: 1,
            [Currency.EUR]: 0.9,
            [Currency.GBP]: 0.9,
        };
        expect(
            FXRates(null, {
                type: UPDATE_FX_RATES,
                payload: rates,
            })
        ).toEqual(rates);

        expect(
            FXRates({
                [Currency.USD]: 1,
                [Currency.EUR]: 1.8,
                [Currency.GBP]: 0.5,
            }, {
                type: UPDATE_FX_RATES,
                payload: rates,
            })
        ).toEqual(rates);
    });
});

describe('moneyBalance reducer', () => {
    const initialState = {
        [Currency.USD]: 100,
        [Currency.GBP]: 120,
        [Currency.EUR]: 80,
    };

    describe('EXCHANGE', () => {
        test('exchange integers', () => {
            expect(
                moneyBalance(initialState, {
                    type: EXCHANGE,
                    payload: {
                        depositMoney: 10,
                        depositCurrency: Currency.EUR,
                        withdrawalMoney: 20,
                        withdrawalCurrency: Currency.GBP,
                    },
                })
            ).toEqual({
                [Currency.USD]: 100,
                [Currency.GBP]: 100,
                [Currency.EUR]: 90,
            });
        });

        test('exchange float', () => {
            expect(
                moneyBalance(initialState, {
                    type: EXCHANGE,
                    payload: {
                        depositMoney: 99.21,
                        depositCurrency: Currency.USD,
                        withdrawalMoney: 55.66,
                        withdrawalCurrency: Currency.EUR,
                    },
                })
            ).toEqual({
                [Currency.USD]: 199.21,
                [Currency.GBP]: 120,
                [Currency.EUR]: 24.34,
            });
        });

        test('exchange float with rounding', () => {
            expect(
                moneyBalance(initialState, {
                    type: EXCHANGE,
                    payload: {
                        depositMoney: 12.12122,
                        depositCurrency: Currency.USD,
                        withdrawalMoney: 45.6789,
                        withdrawalCurrency: Currency.EUR,
                    },
                })
            ).toEqual({
                [Currency.USD]: 112.12,
                [Currency.GBP]: 120,
                [Currency.EUR]: 34.32,
            });
        });

        test('not exchange if currencies are the same', () => {
            expect(
                moneyBalance(initialState, {
                    type: EXCHANGE,
                    payload: {
                        depositMoney: 10,
                        depositCurrency: Currency.EUR,
                        withdrawalMoney: 20,
                        withdrawalCurrency: Currency.EUR,
                    },
                })
            ).toEqual(initialState);
        });

        test('not exchange if withdrawal money more than money balance', () => {
            expect(
                moneyBalance(initialState, {
                    type: EXCHANGE,
                    payload: {
                        depositMoney: 200,
                        depositCurrency: Currency.USD,
                        withdrawalMoney: 100,
                        withdrawalCurrency: Currency.EUR,
                    },
                })
            ).toEqual(initialState);
        });

        test('not exchange if deposit money less than zero', () => {
            expect(
                moneyBalance(initialState, {
                    type: EXCHANGE,
                    payload: {
                        depositMoney: -200,
                        depositCurrency: Currency.USD,
                        withdrawalMoney: 100,
                        withdrawalCurrency: Currency.EUR,
                    },
                })
            ).toEqual(initialState);
        });

        test('not exchange if withdrawal money less than zero', () => {
            expect(
                moneyBalance(initialState, {
                    type: EXCHANGE,
                    payload: {
                        depositMoney: 200,
                        depositCurrency: Currency.USD,
                        withdrawalMoney: -100,
                        withdrawalCurrency: Currency.EUR,
                    },
                })
            ).toEqual(initialState);
        });
    });
});