import {Currency} from '../config';

export type Action = {
    type: string;
    payload?: any;
}

export type MoneyBalance = {
    [Currency.USD]: number;
    [Currency.GBP]: number;
    [Currency.EUR]: number;
}

export type FXRatesType = {
    [Currency.USD]: number;
    [Currency.GBP]: number;
    [Currency.EUR]: number;
} | null;

export type GlobalState = {
    moneyBalance: MoneyBalance;
    FXRates: FXRatesType;
}

export type ExchangeActionPayload = {
    depositMoney: number;
    withdrawalMoney: number;
    withdrawalCurrency: Currency;
    depositCurrency: Currency;
};