import {GlobalState, Action, MoneyBalance, ExchangeActionPayload, FXRatesType} from './types';
import {EXCHANGE, UPDATE_FX_RATES} from './actions';
import {Currency} from '../config';
import {combineReducers} from 'redux';
import {round2decimal} from '../utils';

export const initialState: GlobalState = {
    FXRates: null,
    moneyBalance: {
        [Currency.USD]: 100,
        [Currency.GBP]: 120,
        [Currency.EUR]: 80,
    },
};

export const FXRates = (state: FXRatesType = initialState.FXRates, action: Action) => {
    switch (action.type) {
        case UPDATE_FX_RATES: {
            return {...action.payload};
        }
        default: {
            return state;
        }
    }
};

export const moneyBalance = (state: MoneyBalance = initialState.moneyBalance, action: Action): MoneyBalance => {
    switch (action.type) {
        case EXCHANGE: {
            const {depositMoney, withdrawalMoney, withdrawalCurrency, depositCurrency} = action.payload as ExchangeActionPayload;

            if (withdrawalCurrency === depositCurrency) {
                return state;
            }

            if (depositMoney < 0 || withdrawalMoney < 0) {
                return state;
            }

            const moneyBalanceAfterWithdrawal = state[withdrawalCurrency] - withdrawalMoney;

            if (moneyBalanceAfterWithdrawal < 0) {
                return state;
            }

            const moneyBalanceAfterDeposit = state[depositCurrency] + depositMoney;

            return {
                ...state,
                [withdrawalCurrency]: round2decimal(moneyBalanceAfterWithdrawal),
                [depositCurrency]: round2decimal(moneyBalanceAfterDeposit),
            }
        }
        default: {
            return state;
        }
    }
};

export const reducer = combineReducers({moneyBalance, FXRates});