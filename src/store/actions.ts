import 'cross-fetch/polyfill';
import {ExchangeActionPayload} from './types';
import {FXRatesApi} from '../config';

export const EXCHANGE = 'EXCHANGE';
export const UPDATE_FX_RATES = 'UPDATE_FX_RATES';

export const exchange = (exchangeActionPayload: ExchangeActionPayload) => ({
    type: EXCHANGE,
    payload: exchangeActionPayload,
});

export const updateFXRates = () => async (dispatch: Function) => {
    const response = await fetch(FXRatesApi);
    const {rates, base} = await response.json();

    dispatch({
        type: UPDATE_FX_RATES,
        payload: {[base]: 1, ...rates},
    });
};
