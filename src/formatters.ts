import {Currency} from './config';

const symbols = {
    [Currency.USD]: '$',
    [Currency.EUR]: '€',
    [Currency.GBP]: '£',
};

export const formatMoney = (money: number, currency: Currency) => {
    return `${symbols[currency]}${money}`;
};
