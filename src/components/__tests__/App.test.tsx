import React from 'react';
import {create} from 'react-test-renderer';
import {App} from '../App';
import {shallow, ShallowWrapper} from 'enzyme';
import {Currency} from '../../config';

const props = {
    moneyBalance: {
        [Currency.USD]: 100,
        [Currency.EUR]: 120,
        [Currency.GBP]: 80,
    },
    exchange: jest.fn(),
    availableToTransferCurrencies: [Currency.EUR, Currency.GBP, Currency.USD],
    FXRates:{
        [Currency.USD]: 1.3,
        [Currency.EUR]: 1,
        [Currency.GBP]: 0.8,
    },
    updateFXRates: jest.fn(),
};

const getWithdrawalElement = (wrapper: ShallowWrapper) => wrapper.find('Exchange').at(0) as any;
const getDepositElement = (wrapper: ShallowWrapper) => wrapper.find('Exchange').at(1) as any;
const getExchangeButton = (wrapper: ShallowWrapper) => wrapper.find('button');

describe('App component', () => {
    test('App matches the snapshot', () => {
        const component = create(
            <App {...props} />
        );

        expect(component.toJSON()).toMatchSnapshot();
    });

    test('App not render if FXRates null', () => {
        const component = create(
            <App {...props} FXRates={null} />
        );

        expect(component.toJSON()).toMatchSnapshot();
    });

    test('initial currencies', () => {
        const wrapper = shallow(<App {...props} />);

        expect(getWithdrawalElement(wrapper).props().currency).toEqual(Currency.EUR);
        expect(getDepositElement(wrapper).props().currency).toEqual(Currency.GBP);
    });

    test('update deposit money after change withdrawal money', () => {
        const wrapper = shallow(<App {...props} />);

        getWithdrawalElement(wrapper).props().onChange(20);
        expect(getDepositElement(wrapper).props().moneyToTransfer).toEqual(16);
    });

    test('update withdrawal money', async () => {
        const wrapper = shallow(<App {...props} />);

        getWithdrawalElement(wrapper).props().onChange(50);
        expect(getWithdrawalElement(wrapper).props().moneyToTransfer).toEqual(50);
    });

    test('render right demonstrative rate', async () => {
        const wrapper = shallow(<App {...props} />);

        expect(getDepositElement(wrapper).props().rate).toEqual('€1 ≈ £0.8');
        getDepositElement(wrapper).props().onMoveRight();
        expect(getDepositElement(wrapper).props().rate).toEqual('€1 ≈ $1.3');
    });

    test('update withdrawal currency', async () => {
        const wrapper = shallow(<App {...props} />);

        getWithdrawalElement(wrapper).props().onMoveRight();
        expect(getWithdrawalElement(wrapper).props().currency).toEqual(Currency.GBP);
        getWithdrawalElement(wrapper).props().onMoveRight();
        expect(getWithdrawalElement(wrapper).props().currency).toEqual(Currency.USD);
        getWithdrawalElement(wrapper).props().onMoveRight();
        expect(getWithdrawalElement(wrapper).props().currency).toEqual(Currency.EUR);
        getWithdrawalElement(wrapper).props().onMoveLeft();
        expect(getWithdrawalElement(wrapper).props().currency).toEqual(Currency.USD);
        getWithdrawalElement(wrapper).props().onMoveLeft();
        expect(getWithdrawalElement(wrapper).props().currency).toEqual(Currency.GBP);
    });

    test('update deposit currency', async () => {
        const wrapper = shallow(<App {...props} />);

        getDepositElement(wrapper).props().onMoveRight();
        expect(getDepositElement(wrapper).props().currency).toEqual(Currency.USD);
        getDepositElement(wrapper).props().onMoveRight();
        expect(getDepositElement(wrapper).props().currency).toEqual(Currency.EUR);
        getDepositElement(wrapper).props().onMoveRight();
        expect(getDepositElement(wrapper).props().currency).toEqual(Currency.GBP);
        getDepositElement(wrapper).props().onMoveLeft();
        expect(getDepositElement(wrapper).props().currency).toEqual(Currency.EUR);
        getDepositElement(wrapper).props().onMoveLeft();
        expect(getDepositElement(wrapper).props().currency).toEqual(Currency.USD);
    });

    test('exchange', () => {
        const wrapper = shallow(<App {...props} />);

        getWithdrawalElement(wrapper).props().onChange(10);
        getExchangeButton(wrapper).simulate('click');
        expect(props.exchange.mock.calls[0]).toEqual([{
            depositCurrency: 'GBP',
            depositMoney: 8,
            withdrawalCurrency: 'EUR',
            withdrawalMoney: 10,
        }]);

        getWithdrawalElement(wrapper).props().onChange(20);
        getExchangeButton(wrapper).simulate('click');
        expect(props.exchange.mock.calls[1]).toEqual([{
            depositCurrency: 'GBP',
            depositMoney: 16,
            withdrawalCurrency: 'EUR',
            withdrawalMoney: 20,
        }]);
    });
});