import React from 'react';
import {create} from 'react-test-renderer';
import {Exchange} from '../Exchange';
import {shallow} from 'enzyme';

const props = {
    currency: 'USD',
    total: '$100',
    moneyToTransfer: 10,
    onMoveLeft: jest.fn(),
    onMoveRight: jest.fn(),
    onChange: jest.fn(),
    rate: '€1 ≈ £0.88',
    name: 'test',
};

describe('Exchange component', () => {
    test('Exchange matches the snapshot', () => {
        const component = create(
            <Exchange {...props} />
        );

        expect(component.toJSON()).toMatchSnapshot();
    });

    test('call onMove* after navigation buttons click', () => {
        const wrapper = shallow(
            <Exchange {...props} />
        );

        wrapper.find('button[name="left"]').simulate('click');
        wrapper.find('button[name="right"]').simulate('click');

        expect(props.onMoveLeft.mock.calls.length).toEqual(1);
        expect(props.onMoveRight.mock.calls.length).toEqual(1);
    });

    test('call onChange after change input', () => {
        const wrapper = shallow(
            <Exchange {...props} />
        );

        const input = wrapper.find('input[name="test"]');
        input.simulate('change', { currentTarget: { value: 20, validity: {valid: true} } });

        expect(props.onChange.mock.calls.length).toEqual(1);
        expect(props.onChange.mock.calls[0]).toEqual([20]);
    });
});