import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';

import {Exchange} from './Exchange';
import {ExchangeActionPayload, FXRatesType, GlobalState, MoneyBalance} from '../store/types';
import {exchange, updateFXRates} from '../store/actions';
import {getMoneyBalance, getAvailableToTransferCurrencies, getFXRates} from '../store/selectors';
import {Currency} from '../config';
import {calculateCircularIndex, round2decimal} from '../utils';
import {formatMoney} from '../formatters';

import './App.css';

type Props = {
    moneyBalance: MoneyBalance;
    availableToTransferCurrencies: Currency[];
    exchange: (payload: ExchangeActionPayload) => void;
    FXRates: FXRatesType;
    updateFXRates: () => void;
};

export const App = ({moneyBalance, exchange, availableToTransferCurrencies, FXRates, updateFXRates}: Props) => {
    const [withdrawalMoney, setWithdrawalMoney] = useState(10);
    const [activeWithdrawalIndex, setActiveWithdrawalIndex] = useState(0);
    const [activeDepositIndex, setActiveDepositIndex] = useState(1);
    const activeWithdrawalCurrency = availableToTransferCurrencies[activeWithdrawalIndex];
    const activeDepositCurrency = availableToTransferCurrencies[activeDepositIndex];

    useEffect(() => {
        updateFXRates();
        const timerId = setInterval(updateFXRates, 10000);

        return () => clearInterval(timerId);
    }, [updateFXRates]);

    useEffect(() => {
        if (withdrawalMoney > moneyBalance[activeWithdrawalCurrency]) {
            setWithdrawalMoney(moneyBalance[activeWithdrawalCurrency]);
        }
    }, [moneyBalance, activeWithdrawalCurrency, withdrawalMoney]);

    if (FXRates === null) {
        return null;
    }

    const currencyRate = FXRates[activeDepositCurrency] / FXRates[activeWithdrawalCurrency];
    const depositMoney = withdrawalMoney * currencyRate;
    const demonstrativeRate = `${formatMoney(1, activeWithdrawalCurrency)} ≈ ${formatMoney(round2decimal(currencyRate), activeDepositCurrency)}`;

    const handleExchange = () => {
        exchange({
            depositMoney,
            withdrawalMoney,
            withdrawalCurrency: activeWithdrawalCurrency,
            depositCurrency: activeDepositCurrency,
        })
    };
    const handleChangeWithdrawalCurrency = (newIndex: number) => {
        setActiveWithdrawalIndex(calculateCircularIndex(newIndex, availableToTransferCurrencies));
    };
    const handleChangeDepositCurrency = (newIndex: number) => {
        setActiveDepositIndex(calculateCircularIndex(newIndex, availableToTransferCurrencies));
    };

    return (
        <>
            <div className="header">
                <button
                    onClick={handleExchange}
                    disabled={activeWithdrawalIndex === activeDepositIndex}
                >
                    Exchange
                </button>
            </div>
            <Exchange
                name="withdrawal"
                withAutoFocus
                currency={activeWithdrawalCurrency}
                total={formatMoney(moneyBalance[activeWithdrawalCurrency], activeWithdrawalCurrency)}
                moneyToTransfer={withdrawalMoney}
                onChange={setWithdrawalMoney}
                onMoveLeft={() => handleChangeWithdrawalCurrency(activeWithdrawalIndex - 1)}
                onMoveRight={() => handleChangeWithdrawalCurrency(activeWithdrawalIndex + 1)}
            />
            <Exchange
                name="deposit"
                backgroundColor="#2B5BD0"
                currency={activeDepositCurrency}
                total={formatMoney(moneyBalance[activeDepositCurrency], activeDepositCurrency)}
                moneyToTransfer={round2decimal(depositMoney)}
                onMoveLeft={() => handleChangeDepositCurrency(activeDepositIndex - 1)}
                onMoveRight={() => handleChangeDepositCurrency(activeDepositIndex + 1)}
                rate={demonstrativeRate}
            />
        </>
    );
};

export const ConnectedApp = connect(
    (state: GlobalState) => ({
        moneyBalance: getMoneyBalance(state),
        availableToTransferCurrencies: getAvailableToTransferCurrencies(state),
        FXRates: getFXRates(state),
    }),
    {exchange, updateFXRates}
)(App);