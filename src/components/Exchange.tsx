import React, {useCallback, useRef} from 'react';

import './Exchange.css';

type Props = {
    backgroundColor: string;
    currency: string;
    total: string;
    moneyToTransfer: number;
    onChange: (moneyToTransfer: number) => void;
    onMoveLeft: () => void;
    onMoveRight: () => void;
    withAutoFocus: boolean;
    rate: string | null;
    name?: string;
}

const defaultProps: Partial<Props> = {
    rate: null,
    backgroundColor: '#1E61ED',
    onChange: () => {},
    withAutoFocus: false,
};

export const Exchange = (
    {
        backgroundColor,
        currency,
        total,
        moneyToTransfer,
        onChange,
        onMoveLeft,
        onMoveRight,
        withAutoFocus,
        rate,
        name,
    }: Props
) => {
    const inputElement = useRef(null);

    if (withAutoFocus && inputElement.current) {
        // @ts-ignore inputElement.current is not null here
        inputElement.current.focus();
    }

    let rateBlock;

    if (rate !== null) {
        rateBlock = (
            <div className="rate">
                {rate}
            </div>
        );
    }

    const handleChangeMoney = useCallback((e: React.FormEvent<HTMLInputElement>) => {
        if (e.currentTarget.validity.valid) {
            const value = parseFloat(e.currentTarget.value);

            if (Number.isNaN(value)) {
                onChange(0);

                return;
            }

            onChange(value);
        }
    }, [onChange]);

    return (
        <div className="exchange-wrapper" style={{backgroundColor}}>
            <div className="exchange">
                <div className="block">
                    <div className="currency">
                        {currency}
                    </div>
                    <div className="total">
                        You have {total}
                    </div>
                </div>
                <div className="block right">
                    <input
                        name={name}
                        className="money-to-transfer"
                        type="number"
                        min={0}
                        step={0.01}
                        pattern="^\d+[.,]?\d{0,2}$"
                        value={moneyToTransfer}
                        onChange={handleChangeMoney}
                        autoFocus={withAutoFocus}
                        ref={inputElement}
                    />
                    {rateBlock}
                </div>
            </div>
            <div className="navigation">
                <button name="left" onClick={onMoveLeft}>◀</button>
                <button name="right" onClick={onMoveRight}>▶</button>
            </div>
        </div>
    );
};

Exchange.defaultProps = defaultProps;
