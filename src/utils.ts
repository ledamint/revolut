export const calculateCircularIndex = (index: number, array: any[]) => {
    if (index < 0) {
        return array.length - 1;
    }

    if (index > array.length - 1) {
        return 0;
    }

    return index;
};

export const round2decimal = (num: number) =>  {
    return Math.round(num * 100) / 100
};
